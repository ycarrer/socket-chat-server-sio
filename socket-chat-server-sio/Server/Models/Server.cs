﻿using Newtonsoft.Json;
using ServerApp.Utils.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServerApp.Models
{
    public class Server : AsynchronousSocketListener
    {
        public int Id { get; set; }
        public List<Room> Rooms { get; set; }
        public static List<User> registeredUsers = new List<User>();


        public Server(string ipAdress, int port) : base(ipAdress, port)
        {
        }

        public void Start()
        {
            this.StartListening();
            User test = new User("Yoyo", "test@test.fr", "test123");
            registeredUsers.Add(test);
        }

        private void WatchConnection()
        {

        }

        public void Stop()
        {
            this.StopListening();
        }

        public override void DispatchSocketRequest(Socket socket, SocketRequest request)
        {
            User user = null;
            // Recuperer le user grace dans le dictionnaire

            //if (ConnectedUsers.ContainsKey(socket))
            //{
            //    user = ConnectedUsers[socket];
            //}

            if (request.Command == "register_user")
            {
                HandleRegisterUserCommand(socket, request, user);
            }

            if (request.Command == "login_user")
            {
                HandleLoginUserCommand(request, socket);
            }

            if (request.Command == "logout_user")
            {
                HandleLogOutCommand(request, user);
            }

            if (request.Command == "write_message")
            {
                HandleWriteMessageCommand(request, user);
            }

            if (request.Command == "list_publicroom")
            {
                HandleListPublicRoomCommand(request, user);
            }

            if (request.Command == "update_user")
            {
                HandleUpdateUserCommand(request, user);
            }

            if (request.Command == "update_room")
            {
                HandleUpdateRoomCommand(request, user);
            }

            if (request.Command == "delete_room")
            {
                HandleDeleteRoomCommand(request, user);
            }

            if (request.Command == "kick_user")
            {
                HandleKickUserCommand(request, user);
            }



        }

        private void HandleKickUserCommand(SocketRequest request, User user)
        {
            throw new NotImplementedException();
        }

        private void HandleDeleteRoomCommand(SocketRequest request, User user)
        {
            throw new NotImplementedException();
        }

        private void HandleUpdateRoomCommand(SocketRequest request, User user)
        {
            throw new NotImplementedException();
        }

        private void HandleUpdateUserCommand(SocketRequest request, User user)
        {
            throw new NotImplementedException();
        }

        private void HandleLogOutCommand(SocketRequest request, User user)
        {
            throw new NotImplementedException();
        }

        private void HandleListPublicRoomCommand(SocketRequest request, User user)
        {
            if (user != null)
            {

            }
            else
            {

            }
        }

        private void HandleLoginUserCommand(SocketRequest request, Socket socket)
        {

            // Validation des paramètres
            string email;
            string password;
            if (request.Parameters.Count() == 2)
            {
                if (request.Parameters[0] != null)
                {
                    if (request.Parameters[1] != null)
                    {
                        email = request.Parameters[0].ToString();
                        password = request.Parameters[1].ToString();

                        User foundUser = registeredUsers.FirstOrDefault(x => x.Email == email);

                        if (foundUser != null)
                        {
                            if (foundUser.Password == password)
                            {
                                // UTILISATEUR CONNECTE
                                this.ConnectedUsers.Add(socket, foundUser);
                                string socketResponse = JsonConvert.SerializeObject(new SocketResponse("login_user", foundUser));
                                this.Send(socket, socketResponse);
                            }
                            else
                            {
                                //Erreur mot de passe incorect
                                string socketResponse = JsonConvert.SerializeObject(new SocketResponse("login_user", "Mot de passe incorect"));
                                this.Send(socket, socketResponse);
                            }
                        }
                        else
                        {
                            //Erreur email existe pas
                            string socketResponse = JsonConvert.SerializeObject(new SocketResponse("login_user", "Email inexistant"));
                            this.Send(socket, socketResponse);
                        }
                    }
                    else
                    {
                        //Erreur mot de passe vide
                        string socketResponse = JsonConvert.SerializeObject(new SocketResponse("login_user", "Mot de passe vide"));
                        this.Send(socket, socketResponse);
                    }
                }
                else
                {
                    //Erreur mail vide
                    string socketResponse = JsonConvert.SerializeObject(new SocketResponse("login_user", "Mail vide"));
                    this.Send(socket, socketResponse);
                }
            }
            else
            {
                //Erreur nb de parametres non valide
            }


        }

        private void HandleRegisterUserCommand(Socket socket, SocketRequest request, User user)
        {

            // Validation des paramètres
            string username;
            string email;
            string password;
            if (request.Parameters.Count() == 3)
            {
                if (request.Parameters[0] != null)
                {
                    if (request.Parameters[1] != null)
                    {


                        if (request.Parameters[2] != null)
                        {
                            username = request.Parameters[0].ToString();
                            email = request.Parameters[1].ToString();
                            password = request.Parameters[2].ToString();
                            User newUser = new User(username, email, password);
                            registeredUsers.Add(newUser);
                            string socketResponse = JsonConvert.SerializeObject(new SocketResponse("user_register", newUser));
                            this.Send(socket, socketResponse);
                        }
                        else
                        {
                            //Erreur password vide
                            string socketResponse = JsonConvert.SerializeObject(new SocketResponse("user_register", "Mot de passe vide"));
                            this.Send(socket, socketResponse);
                        }
                    }
                    else
                    {
                        //Erreur email vide
                        string socketResponse = JsonConvert.SerializeObject(new SocketResponse("user_register", "Email vide"));
                        this.Send(socket, socketResponse);
                    }
                }
                else
                {
                    //Erreur username vide
                    string socketResponse = JsonConvert.SerializeObject(new SocketResponse("user_register", "Username vide"));
                    this.Send(socket, socketResponse);
                }
            }
            else
            {
                //Erreur nb de parametres non valide
                string socketResponse = JsonConvert.SerializeObject(new SocketResponse("user_register", "Paramètres non valides."));
                this.Send(socket, socketResponse);
            }


        }

        private void HandleWriteMessageCommand(SocketRequest request, User user)
        {
            if (user != null)
            {
                // Validation des paramètres
                int roomId;
                string message;
                if (request.Parameters.Count() == 2)
                {
                    if (int.TryParse(request.Parameters[0].ToString(), out roomId))
                    {
                        if (request.Parameters[1] != null)
                        {
                            message = request.Parameters[1].ToString();
                            WriteMessage(user, roomId, message);
                        }
                        else
                        {
                            //Erreur message vide
                        }
                    }
                    else
                    {
                        //Erreur id de room n'est pas un nombre
                    }
                }
                else
                {
                    //Erreur nb de parametres non valide
                }
            }
            else
            {
                //Envoyer erreur, utilisateur non authentifier
            }
        }

        private void WriteMessage(User user, int roomId, string message)
        {
            var slot = Rooms[roomId].Slots.Find(x => x.User == user);
            Message msg = new Message(slot, message, DateTime.Now);
            slot.Messages.Add(msg);
        }
    }
}
