﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerApp.Models
{
    public class Slot
    {
        public int Id { get; set; }

        public Room Room { get; set; }
        public User User { get; set; }

        public List<Message> Messages { get; set; }
    }
}
