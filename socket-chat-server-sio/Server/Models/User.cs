﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerApp.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string LastKnownIP { get; set; }
        public int AvatarId { get; set; }

        public List<Slot> Slots { get; set; }

        public User(string name, string email, string password)
        {
            Name = name;
            Email = name;
            Password = password;
        }
    }
}
