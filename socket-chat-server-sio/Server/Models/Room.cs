﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerApp.Models
{
    public class Room
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Password { get; set; }

        public User Owner { get; set; }

        public Server Server { get; set; }
        public List<Slot> Slots { get; set; }
    }
}
