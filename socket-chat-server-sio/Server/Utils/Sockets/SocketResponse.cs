﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerApp.Utils.Sockets
{
    public class SocketResponse
    {
        public string Response { get; set; }
        public object[] Parameters { get; set; }

        public SocketResponse()
        {
        }

        public SocketResponse(string response, params object[] parameters)
        {

        }
    }
}
