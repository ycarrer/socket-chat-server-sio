﻿using Newtonsoft.Json;
using ServerApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServerApp.Utils.Sockets
{
    public abstract class AsynchronousSocketListener : ViewModel
    {
        Socket listener;
        public string IP { get; set; }
        public int Port { get; set; }
        public Dictionary<Socket, User> ConnectedUsers { get; set; }

        private bool enabled;

        public bool Enabled
        {
            get { return enabled; }
            set { enabled = value; OnPropertyChanged(); }
        }




        public AsynchronousSocketListener(string ipAdress, int port)
        {
            IP = ipAdress;
            Port = port;
            
        }


        public void StartListening()
        {
            // Data buffer for incoming data.  
            byte[] bytes = new Byte[1024];



            // Establish the local endpoint for the socket.  
            // The DNS name of the computer  
            // running the listener is "host.contoso.com".  

            IPAddress ipAddress = IPAddress.Parse(IP);
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, Port);

            // Create a TCP/IP socket.  
            listener = new Socket(ipAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and listen for incoming connections.  
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(100);
                Enabled = true;

                // Start an asynchronous socket to listen for connections.  
                Console.WriteLine("Waiting for a connection...");
                listener.BeginAccept(
                    new AsyncCallback(AcceptCallback),
                    listener);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }



        }

        public void StopListening()
        {
            this.Enabled = false;
            listener.Close();
        }

        public void AcceptCallback(IAsyncResult ar)
        {
            if (Enabled)
            {

                // Get the socket that handles the client request.  
                Socket listener = (Socket)ar.AsyncState;
                Socket handler = listener.EndAccept(ar);

                //ConnectedUsers.Add(handler, new User { Name = handler.LocalEndPoint.ToString() } );
                

                // Start an asynchronous socket to listen for connections.  
                Console.WriteLine("Waiting for a connection again...");
                listener.BeginAccept(
                    new AsyncCallback(AcceptCallback),
                    listener);


                // Create the state object.  
                StateObject state = new StateObject();
                state.workSocket = handler;
                handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), state);
            }
        }

        public void ReadCallback(IAsyncResult ar)
        {
            String content = String.Empty;

            // Retrieve the state object and the handler socket  
            // from the asynchronous state object.  
            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.workSocket;
            
            // Read data from the client socket.   
            int bytesRead = handler.EndReceive(ar);

            if (bytesRead > 0)
            {
                // There  might be more data, so store the data received so far.  
                state.sb.Append(Encoding.UTF8.GetString(
                    state.buffer, 0, bytesRead));

                // Check for end-of-file tag. If it is not there, read   
                // more data.  
                content = state.sb.ToString();
                if (content.IndexOf("<EOF>") > -1)
                {
                    
                    // All the data has been read from the   
                    // client. Display it on the console.  
                    Console.WriteLine("Read {0} bytes from socket. \n Data : {1}",
                        content.Length, content);

                    SocketRequest request = null;
                    //Removing End of File "<EOF>" flag from message


                    string jSon = content;
                    jSon = content.Substring(0, content.Length - "<EOF>".Length);


                    request = JsonConvert.DeserializeObject<SocketRequest>(jSon);

                    // Echo the data back to the client.  
                    DispatchSocketRequest(handler, request);
                }
                else
                {
                    // Not all data received. Get more.  
                    handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), state);
                }
            }
        }

        protected void Send(Socket handler, String data)
        {
            // Convert the string data to byte data using ASCII encoding.  
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.  
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.  
                int bytesSent = handler.EndSend(ar);
                Console.WriteLine("Sent {0} bytes to client.", bytesSent);

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public abstract void DispatchSocketRequest(Socket socket, SocketRequest request);

    }



    // State object for reading client data asynchronously  
    public class StateObject
    {
        // Client  socket.  
        public Socket workSocket = null;
        // Size of receive buffer.  
        public const int BufferSize = 1024;
        // Receive buffer.  
        public byte[] buffer = new byte[BufferSize];
        // Received data string.  
        public StringBuilder sb = new StringBuilder();
    }



}
