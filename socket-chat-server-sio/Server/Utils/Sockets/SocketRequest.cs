﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ServerApp.Utils.Sockets
{
    public class SocketRequest
    {
        public string Command { get; set; }
        public object[] Parameters { get; set; }

        public SocketRequest()
        {

        }

        public SocketRequest(string command, params object[] parameters)
        {

        }
    }
}
