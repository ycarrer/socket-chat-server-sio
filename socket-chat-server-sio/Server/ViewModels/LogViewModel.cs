﻿using ServerApp.Models;
using ServerApp.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerApp.ViewModels
{
    public class LogViewModel : ViewModel
    {
        public Server Server { get; set; }

        public LogViewModel(Server server)
        {
            this.Server = server;
        }
    }
}
