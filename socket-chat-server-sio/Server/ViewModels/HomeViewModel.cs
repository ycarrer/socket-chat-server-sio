﻿using ServerApp.Models;
using ServerApp.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ServerApp.ViewModels
{
    public class HomeViewModel : ViewModel
    {
        public Server Server { get; set; }

        public HomeViewModel(Server server)
        {
            this.Server = server;
            ToggleStateCommand = new RelayCommand(ToggleState, CanToggleState);
        }

        private bool CanToggleState()
        {
            return true;
        }

        private void ToggleState()
        {
            if (Server.Enabled)
            {
                Server.Stop();

            }
            else
            {
                Server.Start();
            }
        }

        public RelayCommand ToggleStateCommand { get; set; }

        
    }
}
