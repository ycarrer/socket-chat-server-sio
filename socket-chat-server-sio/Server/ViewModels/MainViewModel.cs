﻿using ServerApp.Models;
using ServerApp.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using ServerApp.Properties;

namespace ServerApp.ViewModels
{
    public class MainViewModel : ViewModel
    {
        public ViewModel[] Children { get; set; }

        private ViewModel currentChild;
        public ViewModel CurrentChild
        {
            get { return currentChild; }
            set { currentChild = value; OnPropertyChanged(); }
        }


        private HomeViewModel homeVM;
        private ManagerViewModel managerVM;
        private SettingsViewModel settingsVM;
        private LogViewModel logVM;

        private Server server;


        public MainViewModel()
        {
            server = new Server(Settings.Default.DefaultIp, Settings.Default.DefaultPort);

            


            homeVM = new HomeViewModel(server);
            managerVM = new ManagerViewModel();
            settingsVM = new SettingsViewModel();
            logVM = new LogViewModel(server);

            Children = new ViewModel[]
            {
                homeVM,
                logVM,
                managerVM,
                settingsVM           
            };

            CurrentChild = homeVM;
        }
    }
}
